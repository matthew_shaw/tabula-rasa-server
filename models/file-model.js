let mongoose = require('mongoose')
    , Schema = mongoose.Schema;

let fileDataSchema = new Schema({
    name: String,
    fileData: [[]],
    headers: [],
    projectId: Schema.Types.ObjectId,
    createdDate: {type: Date, default: Date.now}
});

let FileData = mongoose.model('FileData', fileDataSchema);

exports.FileData = FileData;