let mongoose = require('mongoose');
let Plot = require('../models/plot-model').Plot;
let Project = require('../models/project-model').Project;

let createPlot = function(projectId, plot, callback) {
    let plotDataFields = {
      name: plot.name,
      dataSource: plot.dataSource,
      plotOptions: plot.plotOptions,
      projectId: new mongoose.Types.ObjectId(projectId)
    }
  
    let plotObject = new Plot(plotDataFields);
    console.log("projectId", projectId);
    plotObject.save((err, savedPlotObject) => {
      Project.findById(projectId, (err, project) => {
        project.plots.push(new mongoose.Types.ObjectId(savedPlotObject._id));
        project.save((err, updatedProject) => {
            callback(err, savedPlotObject);
        });
      });
    });
};

let createPlotRouteHandler = (req, res) => {
    createPlot(req.params.projectId, req.body, (err, newPlot) => {        
        if (err) {
            res.status(500).send({msg: "An error occured saving new plot"});
        } else {
            res.json({msg: 'Created new plot'});
        }
        
    });
}

exports.plotRoutes = {
    createPlotRouteHandler: createPlotRouteHandler
};