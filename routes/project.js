let mongoose = require('mongoose');
let Project = require('../models/project-model').Project;
let FileData = require('../models/file-model').FileData;
let Plot = require('../models/plot-model').Plot;

let createProject = (project, callback) => {
    let projectModel = new Project(project);
    projectModel.save((err, savedProject) => {
        callback(err, savedProject);
    });
};

let getProjects = (callback) => {
    Project.find({}, (err, projects) => {
        callback(err, projects);
    });
};

let getProjectById = (projectId, callback) => {
    Project.findById(projectId, (err, project) => {
      if (err) {
          callback({status: 500, msg: "An internal server error occured"}, null);        
      } else {
        if (project) {
          let arrayOfFileIds = [];
          for (let fildId of project.files) {
            arrayOfFileIds.push(mongoose.Types.ObjectId(fildId));
          }
          let arrayOfPlotIds = [];
          for (let plotId of project.plots) {
            arrayOfPlotIds.push(mongoose.Types.ObjectId(plotId));
          }
          FileData.find({
            '_id': { $in: arrayOfFileIds}}, (err, fileDataObjects) => {            
              project['fileObjects'] = JSON.stringify(fileDataObjects);
              Plot.find({
                '_id': { $in: arrayOfPlotIds}}, (err, plotDataObjects) => {
                  let hydratedProject = {
                    name: project.name,
                    description: project.description,
                    createdBy: project.createdBy,
                    fileObjects: fileDataObjects,
                    plotObjects: plotDataObjects,
                    _id: project._id
                  }
                  callback(null, hydratedProject);
                } 
              );
            });
        } else {
            callback({status: 400, msg: "No project could be found with that ID"});
        }
      }    
    });
}

let createProjectRoute = (req, res) => {
    createProject(req.body, (err, newProject) => {
        if (err) {
            res.status(500).send({msg: "Error creating new project"});
        } else {
            res.json(newProject);
        }
    })
};

let getProjectsRoute = (req, res) => {
    getProjects((err, projects) => {
        if (err) {
            res.status(500).send({msg: "Error retrieving projects"});
        } else {
            res.json(projects);
        }
    });
};

let getProjectByIdRoute = (req, res) => {
    getProjectById(req.params.id, (err, project) => {
        if (err) {
            res.status(err.status).send({msg: err.msg});
        } else {
            res.json(project);
        }
    }); 
};

exports.projectRoutes = {
    createProjectRoute: createProjectRoute,
    getProjectsRoute: getProjectsRoute,
    getProjectByIdRoute: getProjectByIdRoute
};