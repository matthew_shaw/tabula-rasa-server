var mongoose = require('mongoose')
, Schema = mongoose.Schema;
//mongoose.connect('mongodb://localhost:27017/tabula-rasa-dev', {useMongoClient: true});

let plotSchema = new Schema({
name: String,
dataSource: String,
plotOptions: {},
createdDate: {type: Date, default: Date.now}
});

var Plot = mongoose.model('Plot', plotSchema);

exports.Plot = Plot;