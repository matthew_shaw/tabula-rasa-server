let mongoose = require('mongoose');
let FileData = require('../models/file-model').FileData;
let Project = require('../models/project-model').Project;

uploadFileData = (projectId, file, callback) => {
    let fileDataFields = {
      name: file.name,
      fileData: file.fileData,
      headers: file.headers,
      projectId: new mongoose.Types.ObjectId(projectId)
    }
  
    let fileDataObject = new FileData(fileDataFields);
    fileDataObject.save((err, savedFileDataObject) => {
      Project.findById(projectId, (err, project) => {
        console.log('project', project);
        project.files.push(new mongoose.Types.ObjectId(savedFileDataObject._id));
        project.save((err, updatedProject) => {
            callback(err, savedFileDataObject);
          
        });
      });    
    });
};

let uploadFileDataRoute = (req, res) => {
    uploadFileData(req.params.projectId, req.body, (err, newFile) => {
        if (err) {
            res.status(500).send({msg: "An error occured saving new plot"});
        } else {
            res.json({msg: 'Created new file'});
        }
    });    
};

exports.uploadFileRoutes = {
    uploadFileDataRoute: uploadFileDataRoute
}