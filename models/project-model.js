var mongoose = require('mongoose')
    , Schema = mongoose.Schema;
//mongoose.connect('mongodb://localhost:27017/tabula-rasa-dev', {useMongoClient: true});

let projectSchema = new Schema({
    name: String,
    description: String,
    files: [],
    plots: [],
    createdBy: String,
    createdDate: {type: Date, default: Date.now}
});

var Project = mongoose.model('Project', projectSchema);

exports.Project = Project
//  = {
//     projectSchema: projectSchema,
//     Project: Project
// };