var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var app = express();
app.set('port', (process.env.PORT || 5000));

var mongoose = require('mongoose');
mongoose.connect(process.env.MONGODB_URI, {useMongoClient: true});

let projectRoutes = require('./routes/project').projectRoutes;
let uploadDataRoutes = require('./routes/upload').uploadFileRoutes
let plotRoutes = require('./routes/plot').plotRoutes;

app.use(cors());
app.use(bodyParser.json());

/** API path that will upload the files */
app.post('/api/project', projectRoutes.createProjectRoute);
app.get('/api/projects', projectRoutes.getProjectsRoute);
app.get('/api/project/:id', projectRoutes.getProjectByIdRoute);
app.post('/api/upload/:projectId', uploadDataRoutes.uploadFileDataRoute);
app.post('/api/plot/:projectId', plotRoutes.createPlotRouteHandler);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("mongodb connection open");  
  app.listen(app.get('port'), function () {
    console.log('CORS-enabled web server listening on port ' + app.get('port'))
  })
});